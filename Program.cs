﻿using CatmeowLib;
using static CatmeowLib.CatmeowLib;
using CatmeowLib.OptionParser;
using System.Text;
using System.Collections.Generic;
using System;

var optionDefs = new OptionDescriptionBase[] {
    new OptionDescription<uint> {
        Description = "The length of the password (Required)",
        LongOpt = "length",
        ShortOpt = 'l',
        Default = 20,
        HasArgument = true,
        Id = "length"
    },
    new OptionDescription<string> {
        Description = "Required characters (Optional)",
        LongOpt = "required-chars",
        ShortOpt = 'r',
        Default = "",
        HasArgument = true,
        Id = "required-chars"
    },
    new OptionDescription<string> {
        Description = "Disallowed characters. Overrides required characters. (Optional)",
        LongOpt = "disallowed-chars",
        ShortOpt = 'd',
        Default = "",
        HasArgument = true,
        Id = "disallowed-chars"
    },

    // Prettier output.
    new OptionDescription<bool> {
        Description = "If specified, enables slow mode, which demonstrates exactly what the program is doing and makes it more pretty.",
        LongOpt = "slow",
        ShortOpt = 's',
        Default = false,
        HasArgument = false,
        Id = "slow",
    },
    new OptionDescription<bool> {
        Description = "Enables or disables colored output.",
        LongOpt = "color",
        ShortOpt = 'c',
        Default = false,
        HasArgument = true,
        Id = "color"
    },

    // Prettiness presets
    new OptionDescription<bool> {
        Description = "Enables prettier output. Enables slow mode and colorful output. This option is ignored when the --boring or -b flags are passed.",
        LongOpt = "pretty",
        Default = false,
        HasArgument = false,
        Id = "pretty"
    },
    new OptionDescription<bool> {
        Description = "Disables default asthetic settings. Also overrides --pretty.",
        LongOpt = "boring",
        ShortOpt = 'b',
        Default = false,
        HasArgument = false,
        Id = "boring"
    }
};
var parser = new OptionsParser(optionDefs).Parse();
uint length = 0;
try {
    parser.DescriptionById<uint>("length").GetValue(ref length);
} catch (OptionUnspecifiedException) {
    Console.Error.WriteLine("ERROR: length must be specified!");
    parser.ShowHelpAndExit(1);
}

// Required and disallowed characters
string requiredChars = parser.DescriptionById<string>("required-chars").GetValueOrNull() ?? "";
string disallowedChars = parser.DescriptionById<string>("disallowed-chars").GetValueOrNull() ?? "";

// Pretiness settings
bool color = parser.DescriptionById<bool>("color").GetValue();
bool slow = parser.DescriptionById<bool>("slow").GetValue();
bool pretty = parser.DescriptionById<bool>("pretty").GetValue();
bool boring = parser.DescriptionById<bool>("boring").GetValue();
if (boring) {
    pretty = false;
}
if (pretty) {
    color = true;
    slow = true;
}
if (!boring) {
    color = true;
}

// The CryptoRandom class doesn't seem to work...
Random cryptoRandom = new Random();
StringBuilder output = new StringBuilder();

// In case the time it takes to generate one character is too long, display some output now.
Console.WriteLine("Generating a random password...");
const ConsoleColor defaultColor = ConsoleColor.White;
const ConsoleColor unprocessedColor = ConsoleColor.DarkGray;
const ConsoleColor processedColor = ConsoleColor.Green;
if (color)
    Console.ForegroundColor = unprocessedColor;

// Generate a random string.
for (int i = 0; i < length; i++) {
    char newChar = CatmeowLib.CatmeowLib.RandCharASCII(cryptoRandom);
    output.Append(newChar);
    Console.Write(newChar);
    if (slow)
        System.Threading.Thread.Sleep(200);
}

#region Required/disallowed character common code
int y = -1;
void setConsoleColor(bool state) {
    Console.ForegroundColor = state ? processedColor : unprocessedColor;
}
void setupConsole() {
    int x = -1;
    (x, y) = Console.GetCursorPosition();
}
void changeColor(bool state, int index) {
    setConsoleColor(state);
    Console.Write(output[index]);
}
void moveOutput(int newLoc) {
    if (y == -1)
        setupConsole();
    Console.SetCursorPosition(newLoc, y);
}
#endregion

// Make sure all required characters are included.
foreach (var required in requiredChars) {
    // Don't include disallowed characters.
    if (disallowedChars.Contains(required)) 
        continue;
    for (var i = 0; i < output.Length; i++) {
        if (output[i] == required) {
            continue;
        }
    }
    int newLoc = CatmeowLib.CatmeowLib.RandInt(cryptoRandom, 0, output.Length - 1);
    // Make sure to keep track of the previous console color
    moveOutput(newLoc);
    output[newLoc] = required;
    changeColor(true, newLoc); // Also updates the character.
    if (slow)
        System.Threading.Thread.Sleep(50);
}

// Remove any disallowed characters.
void doChar(int i) {
    while (disallowedChars.Contains(output[i])) {
        char newChar = CatmeowLib.CatmeowLib.RandCharASCII(cryptoRandom);
        output[i] = newChar;
        moveOutput(i);
        changeColor(true, i);
    }
    if (slow)
        System.Threading.Thread.Sleep(50);
}
if (slow) {
    // Do some extra work to make it prettier.
    List<int> Possibilities = new(output.Length);
    for (int i = 0; i < output.Length; i++) {
        if (disallowedChars.Contains(output[i])) {
            Possibilities.Add(i);
        }
    }
    var basicRandom = new Random();
    while (Possibilities.Count > 0) {
        var changedIndex = basicRandom.Next(0, Possibilities.Count - 1);
        doChar(Possibilities[changedIndex]);
        Possibilities.RemoveAt(changedIndex);
    }
} else {
    // It is not necessary to do any extra decoration.
    for (var i = 0; i < output.Length; i++) {
        doChar(i);
   }
}

// White colored string
if (color)
    Console.ForegroundColor = defaultColor;
Console.Write("\rYour new password is: ");

// Green colored string
if (color)
    Console.ForegroundColor = processedColor;
Console.WriteLine("{0}", output.ToString());

// Make sure to reset the color in case the terminal or shell doesn't do that itself.
if (color)
    Console.ForegroundColor = defaultColor;
